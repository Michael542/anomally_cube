TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    pongjutsu.cpp
INCLUDEPATH += C:/Software/SDL/include
LIBS += -LC:/Software/SDL/lib -lmingw32 -mwindows -mconsole -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf
CXXFLAGS = -std=c++11

HEADERS += \
    pongjutsu.h

