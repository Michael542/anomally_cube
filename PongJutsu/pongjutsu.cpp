
#include "pongjutsu.h"

int error(const char what[]) {
  cout << what << ": " << SDL_GetError() << endl;
  return -1;
}

string IntToString(int n)
{
    std::stringstream sstream;
    sstream << n;
    return sstream.str();
}

SDL_Texture* loadTexture(const string &file, SDL_Renderer *ren){
    SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
    if (texture == NULL)
        error("LoadTexture");
    return texture;
}

Music::Music()
{
    if(SDL_Init(SDL_INIT_AUDIO)==-1) {
        printf("SDL_Init: %s\n", SDL_GetError());
        exit(1);
    }

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)==-1) {
        printf("Mix_OpenAudio: %s\n", Mix_GetError());
        exit(2);
    }
}

Text::Text()
{
    //TXT SETTING
    color = { 255, 255, 255, 255 };
    colorbg = { 0, 0, 0, 0 };
    font = TTF_OpenFont("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\font.ttf", 20);
}

Recta::Recta(int x, int y, int w, int h, int dx = 0, int dy = 0)
{
    R.x = x; R.y = y; R.w = w; R.h = h;
    this->dx = dx;
    this->dy = dy;
}

PongJutsu::PongJutsu()
{
    TTF_Init();

    msc = new Music;
    txt = new Text;

    //BMPnya KOTAK
    bmp = SDL_LoadBMP("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\kotak.bmp");
    bmp = SDL_LoadBMP("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\kotak2.bmp");
    bmp = SDL_LoadBMP("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\kotak4.bmp");
    kotak = SDL_CreateTextureFromSurface(ren, bmp);
    kotak2 = SDL_CreateTextureFromSurface(ren, bmp);
    kotak3 = SDL_CreateTextureFromSurface(ren, bmp);


    msc->bg = Mix_LoadMUS("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\ralph.mp3");
    msc->pantul=Mix_LoadWAV("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\jump.wav");
    if(!msc->pantul) {
        printf("Mix_LoadWAV: %s\n", Mix_GetError());
    }


    //RESOLUSI SETIAP OBJEK

    ke = new Recta(60,70,50,50,10,10);
    pl = new Recta(10,420,150,150);
    pl2 = new Recta(1100,10,128,128);
    bo1 = new Recta(0,0,30,576);
    bo2 = new Recta(1240,0,30,576);
    blok = new Recta(640,0,50,100,0,5);
    tx = new Recta(20,20,20,40);
    tx2 = new Recta(1080,20,20,40);
    tm = new Recta(W/2,20,30,40);
    game_over = new Recta(0,0,1280,576);
    game_over_2 = new Recta(0,0,1280,576);
    game_over_3 = new Recta(0,0,1280,576);

}

int PongJutsu::play()
{
    cout << "starting sdl app.." << endl;

    if(SDL_Init(SDL_INIT_EVERYTHING)) return error("SDL_Init Error");
    win = SDL_CreateWindow("Spacetime Oddysey", 100, 100, W, H, SDL_WINDOW_SHOWN);
    if (!win) return error("SDL_CreateWindow Error"); // http://wiki.libsdl.org/SDL_WindowFlags
    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!ren) return error("SDL_CreateRenderer Error"); // http://wiki.libsdl.org/SDL_RendererFlags
    background = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\space.png", ren);
        if(background == NULL) return 1;

        //PNG LOAD
        enemy = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\enemy.png", ren);
            if(enemy == NULL) return 1;
        protag = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\protag.png", ren);
                if(protag == NULL) return 1;
        bulan = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\Moon.png", ren);
                    if(bulan == NULL) return 1;
        rocket = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\rocket.png", ren);
                    if(rocket == NULL) return 1;
        over = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\game_over_p1.png", ren);
                    if(over == NULL) return 1;
        over2 = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\game_over_p2.png", ren);
                    if(over2 == NULL) return 1;
        over3 = loadTexture("..\\build-Pong_Jutsu-Desktop_Qt_5_2_1_MinGW_32bit-Debug\\game_over_p3.png", ren);
                    if(over3 == NULL) return 1;

    Mix_PlayMusic(msc->bg, -1);

    SDL_Event e;
    bool quit = false;
    while(true) {
      while(SDL_PollEvent(&e)) {
       switch(e.type) {
       case SDL_QUIT:
            quit = true;
            break;
        case SDL_KEYDOWN:
        switch (e.key.keysym.sym){
            case SDLK_o:
                pl2->dy=-7;
                break;
            case SDLK_l:
                pl2->dy=7;
                break;
            case SDLK_w:
                pl->dy=-7;
                break;
            case SDLK_s:
                pl->dy=7;
               break;
            case SDLK_ESCAPE:
              quit=true;
              break;
        }
       }
      }
      if(ke->R.x<=0) ke->dx = -ke->dx;
      if(ke->R.x>W-ke->R.w-1) ke->dx = -ke->dx;
      if(ke->R.y<=0) ke->dy = -ke->dy;
      if(ke->R.y>H-ke->R.h-1) ke->dy = -ke->dy;
      ke->R.x += ke->dx;
      ke->R.y += ke->dy;

      if(blok->R.x<=0) blok->dy = -blok->dy;
      if(blok->R.y<0) blok->dy = -blok->dy;
      if(blok->R.y>H-ke->R.h-1) blok->dy = -blok->dy;

      blok->R.y += blok->dy;
      if((pl2->R.y+pl2->dy>=0)&&(pl2->R.y+pl2->dy<=450))
          pl2->R.y += pl2->dy;
      if((pl->R.y+pl->dy>=0)&&(pl->R.y+pl->dy<=440))
          pl->R.y += pl->dy;
      if(quit) break;

      if(SDL_HasIntersection(&pl->R, &ke->R)) {
          if(Mix_PlayChannel(0, msc->pantul, 0)==-1) {
              printf("Mix_PlayChannel: %s\n",Mix_GetError());
          }
          ke->dy = ke->dy;
          ke->dx = -ke->dx;
      };
      if(SDL_HasIntersection(&pl2->R, &ke->R)) {
          if(Mix_PlayChannel(0, msc->pantul, 0)==-1) {
              printf("Mix_PlayChannel: %s\n",Mix_GetError());
          }
          ke->dy = ke->dy;
          ke->dx = -ke->dx;
      };
      if(SDL_HasIntersection(&blok->R, &ke->R)) {
          if(Mix_PlayChannel(0, msc->pantul, 0)==-1) {
              printf("Mix_PlayChannel: %s\n",Mix_GetError());
          }
          ke->dy = ke->dy;
          ke->dx = -ke->dx;
      };

      if(SDL_HasIntersection(&bo1->R, &ke->R)) {
          if(Mix_PlayChannel(0, msc->pantul, 0)==-1) {
              printf("Mix_PlayChannel: %s\n",Mix_GetError());
          }
          ke->dy = ke->dy;
          ke->dx = -ke->dx;

          playerScore2++;
      };

      if(SDL_HasIntersection(&bo2->R, &ke->R)) {
          if(Mix_PlayChannel(0, msc->pantul, 0)==-1) {
              printf("Mix_PlayChannel: %s\n",Mix_GetError());
          }
          ke->dy = ke->dy;
          ke->dx = -ke->dx;

          playerScore++;
      };


      txt->timeSurface = TTF_RenderText_Shaded(txt->font, itoa((SDL_GetTicks()/1000),time,10),txt->color, txt->colorbg );
      txt->timeImage = SDL_CreateTextureFromSurface(ren, txt->timeSurface);
      txt->textSurface = TTF_RenderText_Shaded(txt->font, itoa(playerScore,score1,10), txt->color, txt->colorbg );
      txt->myScoreImage = SDL_CreateTextureFromSurface(ren, txt->textSurface);
      txt->textSurface2 = TTF_RenderText_Shaded(txt->font, itoa(playerScore2,score2,10), txt->color, txt->colorbg );
      txt->myScoreImage2 = SDL_CreateTextureFromSurface(ren, txt->textSurface2);
      SDL_RenderClear(ren); // http://wiki.libsdl.org/moin.fcg/SDL_RenderClear
      SDL_RenderCopy(ren, background, 0, 0); // http://wiki.libsdl.org/moin.fcg/SDL_RenderCopy
      SDL_RenderCopy(ren, bulan, 0, &ke->R);
      SDL_RenderCopy(ren, protag, 0, &pl->R);
      SDL_RenderCopy(ren, enemy, 0, &pl2->R);
      SDL_RenderCopy(ren, kotak, 0, &bo1->R);
      SDL_RenderCopy(ren, kotak2, 0, &bo2->R);
      SDL_RenderCopy(ren, rocket, 0, &blok->R);
      SDL_RenderCopy(ren, txt->myScoreImage, 0, &tx->R);
      SDL_RenderCopy(ren, txt->myScoreImage2, 0, &tx2->R);
      SDL_RenderCopy(ren, txt->timeImage, 0, &tm->R);
      SDL_RenderPresent(ren);
      SDL_Delay(10);
      SDL_Event a;
      if((SDL_GetTicks()/1000)==120){
          if(playerScore>playerScore2){
              SDL_RenderClear(ren);
              SDL_RenderCopy(ren, over, 0, &game_over->R);
              SDL_RenderPresent(ren);
              while(true) {
                while(SDL_PollEvent(&a)) {
                 switch(a.type) {
                 case SDL_QUIT:
                      quit = true;
                      return 0;
                      break;
                  case SDL_KEYDOWN:
                  switch (a.key.keysym.sym){
                       case SDLK_ESCAPE:
                        quit=true;
                        return 0;
                        break;

                  }
                 }
                }
          }
          }
           else if(playerScore<playerScore2){
              SDL_RenderClear(ren);
              SDL_RenderCopy(ren, over2, 0, &game_over_2->R);
              SDL_RenderPresent(ren);
              while(true) {
                while(SDL_PollEvent(&a)) {
                    switch(a.type) {
                    case SDL_QUIT:
                         quit = true;
                         return 0;
                         break;
                     case SDL_KEYDOWN:
                     switch (a.key.keysym.sym){
                          case SDLK_ESCAPE:
                           quit=true;
                           return 0;
                           break;

                  }
                 }
                }
          }
          }
           else if(playerScore==playerScore2){
              SDL_RenderClear(ren);
              SDL_RenderCopy(ren, over3, 0, &game_over_3->R);
              SDL_RenderPresent(ren);
              while(true) {
                while(SDL_PollEvent(&a)) {
                 switch(a.type) {
                 case SDL_QUIT:
                      quit = true;
                      return 0;
                      break;
                  case SDL_KEYDOWN:
                  switch (a.key.keysym.sym){
                      case SDLK_ESCAPE:
                        quit=true;
                        return 0;
                        break;

                  }
                 }
                }
              }
          }
      }
    }
    SDL_DestroyTexture(background);
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    Mix_FreeChunk(msc->pantul);

    SDL_Quit();
    cout << "exiting sdl app.." << endl;
}
