#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include <string>
#include <stdlib.h>
using namespace std;
#undef main

int playerScore = 0, playerScore2 = 0;
char score1[10];
char score2[10];
char time [10];

int error(const char what[]) {
  cout << what << ": " << SDL_GetError() << endl;
  return -1;
}

string IntToString(int n)
{
    std::stringstream sstream;
    sstream << n;
    return sstream.str();
}

SDL_Texture* loadTexture(const string &file, SDL_Renderer *ren){
    SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
    if (texture == NULL)
        error("LoadTexture");
    return texture;
}

string text;

int main() {
  TTF_Init();
  cout << "starting sdl app.." << endl;
  const int W = 1280, H = 576;
  if(SDL_Init(SDL_INIT_EVERYTHING)) return error("SDL_Init Error");
  SDL_Window *win = SDL_CreateWindow("Spacetime Oddysey", 100, 100, W, H, SDL_WINDOW_SHOWN);
  if (!win) return error("SDL_CreateWindow Error"); // http://wiki.libsdl.org/SDL_WindowFlags
  SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (!ren) return error("SDL_CreateRenderer Error"); // http://wiki.libsdl.org/SDL_RendererFlags
  SDL_Texture *background = loadTexture("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\space.png", ren);
      if(background == NULL) return 1;

  //BMPnya KOTAK
  SDL_Surface *bmp;
  bmp = SDL_LoadBMP("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\kotak.bmp");
  bmp = SDL_LoadBMP("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\kotak2.bmp");
  SDL_Texture *kotak = SDL_CreateTextureFromSurface(ren, bmp);
  SDL_Texture *kotak2 = SDL_CreateTextureFromSurface(ren, bmp);


  //TXT SETTING
  SDL_Color color = { 255, 255, 255, 255 };
  SDL_Color colorbg = { 0, 0, 0, 0 };
  TTF_Font *font;
  font = TTF_OpenFont("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\font.ttf", 20);
  SDL_Surface* textSurface;
  SDL_Texture *myScoreImage;
  SDL_Surface* textSurface2;
  SDL_Texture *myScoreImage2;
  SDL_Surface* timeSurface;
  SDL_Texture *timeImage;

  //PNG LOAD
  SDL_Texture *enemy = loadTexture("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\enemy.png", ren);
      if(enemy == NULL) return 1;
  SDL_Texture *protag = loadTexture("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\protag.png", ren);
          if(protag == NULL) return 1;
  SDL_Texture *bulan = loadTexture("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\Moon.png", ren);
              if(bulan == NULL) return 1;
  SDL_Texture *rocket = loadTexture("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\rocket.png", ren);
              if(rocket == NULL) return 1;

  //Music and CHunk
  Mix_Music *bg;
  Mix_Chunk *pantul=NULL;


              if(SDL_Init(SDL_INIT_AUDIO)==-1) {
                  printf("SDL_Init: %s\n", SDL_GetError());
                  exit(1);
              }

              if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)==-1) {
                  printf("Mix_OpenAudio: %s\n", Mix_GetError());
                  exit(2);
              }

bg = Mix_LoadMUS("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\ralph.mp3");
              pantul=Mix_LoadWAV("C:\\Users\\Michael\\Desktop\\AC\\anomally_cube\\jump.wav");
              if(!pantul) {
                  printf("Mix_LoadWAV: %s\n", Mix_GetError());
              }

              Mix_PlayMusic(bg, -1);

  //RESOLUSI SETIAP OBJEK
  int dp2y = 0,dpy = 0;
  SDL_Rect ke;
  ke.w = 50;
  ke.h = 50;
  ke.x = 60;
  ke.y = 70;
  int dx = 10, dy = 10;

  SDL_Rect pl;
  pl.w = 150;
  pl.h = 150;
  pl.x = 5;
  pl.y = 420;

  SDL_Rect pl2;
  pl2.w = 128;
  pl2.h = 128;
  pl2.x = 1132;
  pl2.y = 10;

  SDL_Rect bo1;
  bo1.w = 30;
  bo1.h = H;
  bo1.x = 0;
  bo1.y = 0;

  SDL_Rect bo2;
  bo2.w = 30;
  bo2.h = H;
  bo2.x = 1250;
  bo2.y = 0;

  SDL_Rect blok;
  blok.w = 50;
  blok.h = 100;
  blok.x = 640;
  blok.y = 0;
  int dy2 = 5;

  SDL_Rect tx;
  tx.x = 20;
  tx.y = 20;
  tx.w = 20;
  tx.h = 40;

  SDL_Rect tx2;
  tx2.x = 1080;
  tx2.y = 20;
  tx2.w = 20;
  tx2.h = 40;

  SDL_Rect tm;
  tm.x = W/2;
  tm.y = 20;
  tm.w = 30;
  tm.h = 40;

  SDL_Event e;
  bool quit = false;
  while(true) {
    while(SDL_PollEvent(&e)) {
     switch(e.type) {
     case SDL_QUIT:
          quit = true;
          break;
      case SDL_KEYDOWN:
      switch (e.key.keysym.sym){
          case SDLK_o:
              dp2y=-7;
              break;
          case SDLK_l:
              dp2y=7;
              break;
          case SDLK_w:
              dpy=-7;
              break;
          case SDLK_s:
              dpy=7;
             break;
          case SDLK_ESCAPE:
            quit=true;
            break;
      }
     }
    }
    if(ke.x<=0) dx = -dx;
    if(ke.x>W-ke.w-1) dx = -dx;
    if(ke.y<=0) dy = -dy;
    if(ke.y>H-ke.h-1) dy = -dy;
    ke.x += dx;
    ke.y += dy;

    if(blok.x<=0) dy2 = -dy2;
    if(blok.y<0) dy2 = -dy2;
    if(blok.y>H-ke.h-1) dy2 = -dy2;

    blok.y += dy2;
    if((pl2.y+dp2y>=0)&&(pl2.y+dp2y<=450))
        pl2.y += dp2y;
    if((pl.y+dpy>=0)&&(pl.y+dpy<=440))
        pl.y += dpy;
    if(quit) break;

    if(SDL_HasIntersection(&pl, &ke)) {
        if(Mix_PlayChannel(0, pantul, 0)==-1) {
            printf("Mix_PlayChannel: %s\n",Mix_GetError());
        }
        dy = dy;
        dx = -dx;
    };
    if(SDL_HasIntersection(&pl2, &ke)) {
        if(Mix_PlayChannel(0, pantul, 0)==-1) {
            printf("Mix_PlayChannel: %s\n",Mix_GetError());
        }
        dy = dy;
        dx = -dx;
    };
    if(SDL_HasIntersection(&blok, &ke)) {
        if(Mix_PlayChannel(0, pantul, 0)==-1) {
            printf("Mix_PlayChannel: %s\n",Mix_GetError());
        }
        dy = dy;
        dx = -dx;
    };

    if(SDL_HasIntersection(&bo1, &ke)) {
        if(Mix_PlayChannel(0, pantul, 0)==-1) {
            printf("Mix_PlayChannel: %s\n",Mix_GetError());
        }
        dy = dy;
        dx = -dx;
        playerScore2++;
    };
    if(SDL_HasIntersection(&bo2, &ke)) {
        if(Mix_PlayChannel(0, pantul, 0)==-1) {
            printf("Mix_PlayChannel: %s\n",Mix_GetError());
        }
        dy = dy;
        dx = -dx;
        playerScore++;
    };

    timeSurface = TTF_RenderText_Shaded(font, itoa((SDL_GetTicks()/1000),time,10), color, colorbg );
    timeImage = SDL_CreateTextureFromSurface(ren, timeSurface);
    textSurface = TTF_RenderText_Shaded(font, itoa(playerScore,score1,10), color, colorbg );
    myScoreImage = SDL_CreateTextureFromSurface(ren, textSurface);
    textSurface2 = TTF_RenderText_Shaded(font, itoa(playerScore2,score2,10), color, colorbg );
    myScoreImage2 = SDL_CreateTextureFromSurface(ren, textSurface2);
    SDL_RenderClear(ren); // http://wiki.libsdl.org/moin.fcg/SDL_RenderClear
    SDL_RenderCopy(ren, background, 0, 0); // http://wiki.libsdl.org/moin.fcg/SDL_RenderCopy
    SDL_RenderCopy(ren, bulan, 0, &ke);
    SDL_RenderCopy(ren, protag, 0, &pl);
    SDL_RenderCopy(ren, enemy, 0, &pl2);
    SDL_RenderCopy(ren, kotak, 0, &bo1);
    SDL_RenderCopy(ren, kotak2, 0, &bo2);
    SDL_RenderCopy(ren, rocket, 0, &blok);
    SDL_RenderCopy(ren, myScoreImage, 0, &tx);
    SDL_RenderCopy(ren, myScoreImage2, 0, &tx2);
    SDL_RenderCopy(ren, timeImage, 0, &tm);
    SDL_RenderPresent(ren);
    SDL_Delay(10);

  }
  SDL_DestroyTexture(background);
  SDL_DestroyRenderer(ren);
  SDL_DestroyWindow(win);
  Mix_FreeChunk(pantul);

  SDL_Quit();
  cout << "exiting sdl app.." << endl;
  return 0;
}
