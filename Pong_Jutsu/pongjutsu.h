#ifndef PONGJUTSU_H
#define PONGJUTSU_H

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include <string>
#include <stdlib.h>
using namespace std;
#undef main

const int W = 1280, H = 576;

class Music
{
    public:
        //Music and CHunk
        Mix_Music *bg;
        Mix_Chunk *pantul;
        Music();
};

class Text
{
    public:
        Text();
        SDL_Color color;
        SDL_Color colorbg;
        TTF_Font *font;
        SDL_Surface* textSurface;
        SDL_Texture *myScoreImage;
        SDL_Surface* textSurface2;
        SDL_Texture *myScoreImage2;
        SDL_Texture *timeImage;
        SDL_Surface* timeSurface;
};

class Recta
{
    public:
        Recta(int,int,int,int,int,int);
        SDL_Rect R;
        int dx,dy;
};

class PongJutsu
{
    int playerScore = 0, playerScore2 = 0;
    char score1[10];
    char score2[10];
    char time[10];

    string text;

    Music *msc;
    Text *txt;

    SDL_Window *win;
    SDL_Renderer *ren;
    SDL_Texture *background,*kotak,*kotak2,*kotak3,*enemy,*protag,*bulan,*rocket,*over,*over2,*over3;
    SDL_Surface *bmp;
    Recta *ke,*pl,*pl2,*bo1,*bo2,*blok,*tx,*tx2,*tm,*game_over,*game_over_2,*game_over_3;
public:
    PongJutsu();

    int play();
};

#endif // PONGJUTSU_H

